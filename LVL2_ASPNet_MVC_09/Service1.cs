﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace LVL2_ASPNet_MVC_09
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                /*
                ThreadStart start = new ThreadStart(Working);
                Worker = new Thread(start);
                Worker.Start();
                */
                tmrExecutor.Elapsed += new ElapsedEventHandler(Working); // adding event
                tmrExecutor.Interval = 60000; // set your time here, 1 sec = 1000
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Working(object sender, System.Timers.ElapsedEventArgs e)
        {
            /*
            while (true)
            {
                string path = "C:\\novan.txt";
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(string.Format("Windows Service is Called on " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                    writer.Close();
                }
                Thread.Sleep(ScheduleTime * 60 * 1000);
            }
            */
            string path = "C:\\novan.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format("Windows Service is Called on " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                writer.Close();
            }
            Thread.Sleep(ScheduleTime * 60 * 1000);
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
                /*
                if ((Worker != null) && Worker.IsAlive)
                {
                    Worker.Abort();
                }
                */
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
